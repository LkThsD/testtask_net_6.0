﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TestTaskConsoleApp.Enums;
using TestTaskConsoleApp.Helpers;
using TestTaskConsoleApp.Interfaces;

namespace TestTaskConsoleApp.Services;

public class NetSdrClient
{
    private ITcpClient _client { get; set; }

    private bool _isIqTransfer = false;

    private CancellationTokenSource _cts;

    private string _outputFilePath = "received_data.txt";

    public NetSdrClient(ITcpClient client)
    {
        _client = client;
    }

    public async Task Connect()
    {
        try
        {
            await _client.ConnectAsync();
            Console.WriteLine("Підключено до NetSDR.");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Помилка при підключенні: " + ex.Message);
        }
    }

    public void Disconnect()
    {
        _client.CloseConnection();
        _isIqTransfer = false;
        Console.WriteLine("Відключено від NetSDR.");
    }

    public async Task StartStopIQTransfer()
    {
        if (_client.IsClientConnected())
            return;

        var command = _isIqTransfer ? NetSdrHelper.GetStopIQTransferCommand() : NetSdrHelper.GetStartIQTransferCommand();
        var result = await _client.SendCommandAsync(command, 6);

        CheckResultOfCommand(result);
    }

    public async Task ChangeReceiverFrequencyRate(ChannelFreqEnum channel, int frequency)
    {
        if (_client.IsClientConnected())
            return;

        var сommand = NetSdrHelper.GetSetFrequencyCommand(channel, frequency);
        var result = await _client.SendCommandAsync(сommand, 10);

        CheckResultOfCommand(result);
    }

    public void StartRecieving(int udpPort = 60000)
    {
        _cts = new CancellationTokenSource();

        Task.Run(async () => await ReceiveDataAsync(udpPort, _cts.Token));
        Console.WriteLine($"Прослуховування NetSDR на порту {udpPort} запущено.");
    }

    public void StopReceiving()
    {
        _cts.Cancel();
    }

    private async Task ReceiveDataAsync(int udpPort, CancellationToken token)
    {
        using (UdpClient udpClient = new UdpClient(udpPort))
        using (StreamWriter writer = new StreamWriter(_outputFilePath))
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, udpPort);
                    UdpReceiveResult result = await udpClient.ReceiveAsync();

                    int sequenceNumber = BitConverter.ToUInt16(result.Buffer, 1);
                    byte[] data = result.Buffer.Skip(3).ToArray();
                    string hexData = BitConverter.ToString(data);

                    writer.WriteLine($"Порядковий номер: {sequenceNumber}, Інформація: {hexData}");
                    writer.Flush();

                    Console.WriteLine($"Отриманна інформація від {remoteEndPoint}: Порядковий номер: {sequenceNumber}");
                }
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Прослуховування NetSDR було зупинено.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Помилка: " + ex.Message);
            }
        }
    }

    private void CheckResultOfCommand(bool result)
    {
        if (result)
        {
            Console.WriteLine($"Стан передачі успішно змінено на {!_isIqTransfer}.");
            _isIqTransfer = !_isIqTransfer;
        }
        else
        {
            Console.WriteLine($"Виникла непердбачувана помилка при зміні стану.");
            throw new Exception("I/Q transfer not changed");
        }
    }
}
