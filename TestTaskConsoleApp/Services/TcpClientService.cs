﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TestTaskConsoleApp.Interfaces;

namespace TestTaskConsoleApp.Services
{
    public class TcpClientService : ITcpClient
    {
        private string _ipAddress { get; set; }
        private int _port { get; set; }
        private TcpClient _client { get; set; }
        private NetworkStream _stream { get; set; }

        public TcpClientService(string ipAddress, int port = 50000)
        {
            _ipAddress = ipAddress;
            _port = port;
            _client = new TcpClient();
        }

        public async Task ConnectAsync()
        {
            await _client.ConnectAsync(IPAddress.Parse(_ipAddress), _port);
            if (IsClientConnected())
            {
                _stream = _client.GetStream();
            }
        }
        public void CloseConnection()
        {
            if (!IsClientConnected())
                return;

            _client.Close();
        }
        public async Task<bool> SendCommandAsync(byte[] command, int responseEqualFields)
        {
            await _stream.WriteAsync(command);

            byte[] response = new byte[command.Length];
            await _stream.ReadAsync(response);

            return CheckResponse(responseEqualFields, command, response);
        }

        public bool IsClientConnected()
        {
            if (!_client.Connected)
            {
                Console.WriteLine("Не пікдлючено до NetSDR.");
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckResponse(int countOfEqualBytes, byte[ ]command, byte[] response)
        {
            bool areEqual = true;
            for (int i = 0; i < countOfEqualBytes; i++)
            {
                if(command[i] != response[i])
                {
                    areEqual = false;
                }
            }

            return areEqual;
        }
    }
}
