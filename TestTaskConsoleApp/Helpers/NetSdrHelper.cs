﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTaskConsoleApp.Enums;

namespace TestTaskConsoleApp.Helpers;

public class NetSdrHelper
{
    public static byte[] GetStartIQTransferCommand()
    {
        byte[] byteCommand = new byte[8];

        // заголовок
        byteCommand[0] = 0x08; // довжина(8 меньш значущих бітів) 0000 1000
        byteCommand[1] = 0x00; // тип повідомлення(перші 3 біти) + довжина(останні 5 більш значуших бітів) 0000 0000

        // код елементу керування
        byteCommand[2] = 0x18; // друга частина коду елементу керування 0х0018
        byteCommand[3] = 0x00; // перша частина коде елементу керування 0х0018

        // параметри запиту
        byteCommand[4] = 0x80; // комплексний режим даних модулюючого сигналу I/Q 1000 0000
        byteCommand[5] = 0x02; // запуск збору даних через порд UDP
        byteCommand[6] = 0x80; // встановлення 24 бітного безперервного режиму передачі даних
        byteCommand[7] = 0x00; // кількість 16-бітних виборок для захвату до відправлення в режимі FIFO

        return byteCommand;
    }

    public static byte[] GetStopIQTransferCommand()
    {
        byte[] byteCommand = new byte[8];

        // заголовок
        byteCommand[0] = 0x08; // довжина(8 меньш значущих бітів) 0000 1000
        byteCommand[1] = 0x00; // тип повідомлення(перші 3 біти) + довжина(останні 5 більш значуших бітів) 0000 0000

        // код елементу керування
        byteCommand[2] = 0x18; // друга частина коду елементу керування 0х0018
        byteCommand[3] = 0x00; // перша частина коде елементу керування 0х0018

        // параметри запиту
        byteCommand[4] = 0x00; // для команди зупину це поле ігнорується
        byteCommand[5] = 0x01; // запуск збору даних через порд UDP
        byteCommand[6] = 0x00; // для команди зупину це поле ігнорується
        byteCommand[7] = 0x00; // для команди зупину це поле ігнорується

        return byteCommand;
    }

    public static byte[] GetSetFrequencyCommand(ChannelFreqEnum channel, int frequencyParam) // значення передається в Гц
    {
        byte[] byteCommand = new byte[10];

        // заголовок
        byteCommand[0] = 0x0A; // довжина(8 меньш значущих бітів) 0000 1010
        byteCommand[1] = 0x00; // тип повідомлення(перші 3 біти) + довжина(останні 5 більш значуших бітів) 0000 0000

        // код елементу керування
        byteCommand[2] = 0x20; // друга частина коду елементу керування 0х0020
        byteCommand[3] = 0x00; // перша частина коде елементу керування 0х0020

        // параметри запиту
        byteCommand[4] = (byte)channel; // встановлення каналу для зміни

        // 5 байтів відповідають за частоту, конвертуємо отримане значення з цілочисельного у байти, інвернуємо і записуємо в массив команди
        byte[] HzByteValueArray = BitConverter.GetBytes(frequencyParam);
        HzByteValueArray.Reverse(); // LittleEndian

        for (int i = 0; i < 5; i++)
        {
            byteCommand[5 + i] = HzByteValueArray[i];
        }

        return byteCommand;
    }


    byte x = 0b00001111;
    public byte x12x { get; set; } = 0x23;
}
