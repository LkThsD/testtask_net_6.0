﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TestTaskConsoleApp.Interfaces;

public interface ITcpClient
{
    public Task ConnectAsync();
    public void CloseConnection();
    public bool IsClientConnected();
    public Task<bool> SendCommandAsync(byte[] command, int responseEqualFields);
}
