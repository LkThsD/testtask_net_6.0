﻿// See https://aka.ms/new-console-template for more information
using TestTaskConsoleApp.Enums;
using TestTaskConsoleApp.Services;

// Приклад використання створеного класу
var tcpService = new TcpClientService("192.168.0.1");
var client = new NetSdrClient(tcpService);

await client.Connect();

var channel = ChannelFreqEnum.Channel0;
await client.ChangeReceiverFrequencyRate(channel, 14010000);

await client.StartStopIQTransfer();

client.StartRecieving();
await Task.Delay(10000);
client.StopReceiving();

await client.StartStopIQTransfer();

client.Disconnect();


