using TestTaskConsoleApp.Enums;

namespace TestTaskConsoleApp.Tests;

public class NetSdrClientTests
{
    private readonly NetSdrClient _client;
    private readonly Mock<ITcpClient> _mockTcpClient = new();
    private byte[] commandExample;

    public NetSdrClientTests()
    {
        _client = new NetSdrClient(_mockTcpClient.Object);
    }

    [Fact]
    public async void NetSdrConnect()
    {
        _mockTcpClient.Setup(mock => mock.ConnectAsync())
            .Returns(Task.CompletedTask);

        await _client.Connect();
    }

    [Fact]
    public void NetSdrDisconnect()
    {
        _client.Disconnect();
    }

    [Fact]
    public async void NetSdrChangeReceiverFrequencyRate()
    {
        _mockTcpClient.Setup(mock => mock.IsClientConnected())
            .Returns(true);
        _mockTcpClient.Setup(mock => mock.SendCommandAsync(commandExample, 1))
            .Returns(Task.FromResult(true));

        var channel = ChannelFreqEnum.Channel0;
        var newFrequency = 14010000;

        await _client.ChangeReceiverFrequencyRate(channel, newFrequency);
    }

    [Fact]
    public async void NetSdrStartStopIQTransfer()
    {
        _mockTcpClient.Setup(mock => mock.IsClientConnected())
            .Returns(true);
        _mockTcpClient.Setup(mock => mock.SendCommandAsync(commandExample, 1))
            .Returns(Task.FromResult(true));

        await _client.StartStopIQTransfer();
        await _client.StartStopIQTransfer();
    }

}